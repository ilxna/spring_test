package com.how2java.springboot.web;

import com.how2java.springboot.pojo.Product;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Controller
public class TestController {

    @RequestMapping("/test")
    public String test(Model model){
        String htmlContent="<p style='color:red'> 红色蚊子</p>";

        Product currentProduct=new Product(5,"product",200);

        boolean testBoolean=true;

        Date now=new Date();
        List<Product>  productList=new ArrayList<>();
        productList.add(new Product(1,"a",50));
        productList.add(new Product(2,"product b",100));
        productList.add(new Product(3,"product c",150));
        productList.add(currentProduct);
        productList.add(new Product(6,"product f",200));

        model.addAttribute("now",now);
        model.addAttribute("ps",productList);
        model.addAttribute("htmlContent",htmlContent);
        model.addAttribute("currentProduct",currentProduct);
        model.addAttribute("testBoolean",testBoolean);
        return "test";
    }
}
