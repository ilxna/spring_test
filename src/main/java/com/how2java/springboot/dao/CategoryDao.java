package com.how2java.springboot.dao;

import  org.springframework.data.jpa.repository.JpaRepository;
import  com.how2java.springboot.pojo.Category;

import java.util.List;

public interface CategoryDao extends  JpaRepository<Category,Integer>{

    public List<Category> findByName(String name);

    public List<Category> findByNameLikeAndIdGreaterThanOrderByNameAsc(String name, int id);
}
